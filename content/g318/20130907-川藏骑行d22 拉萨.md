---
title: 川藏骑行Day22 拉萨
date: 2013-08-18
categories:
  - 318
tags:
  - 骑行
  - 米拉山
  - 拉萨
---

*松多-拉萨*

![](/images/2013/318d22-布达拉.jpg)

5点多起床，百十号人住的地方就两个坑，不分男女，一早就排起长龙。  

更悲剧的是准备泡面的热水刚烧一半就停电了，而且是整个镇子停电。只好跑厨房去泡面，在烛光中吃完早晨。出门时天还很黑，骑着骑着天渐渐亮了。

骑行20多公里到达米拉山口，海拔5013，这是川藏线上最后一座也是最高的一座。天有些阴，到达垭口竟然开始下起小雨，爬坡出了不少汗，停下来有些冷。

到达拉萨还有150多公里，不敢多停，一路缓下。半路遇到狂风，吹的车子都开始左右摇摆。 因为是最后一天了，每个人都象疯狗一样向拉萨扑去，速度很快，六点多就到了。

见到了布达拉宫，高兴，激动。同时也有些失落。 这么说也许有些矫情，但是真的。

休息一天，准备回家。

[完]