---
title: Hack MSN Space
date: 2005-11-10T01:55:08+00:00
categories:
  - 生活

---

MS这次一共推出了3个模块，“Media Player”，“自定义Html模块，“Msn Space 界面设定”。

使用方法如下：

1、在编辑状态下，在地址栏上显示的地址后添加以下代码后回车&powertoy=musicvideo  //Media Player

&powertoy=sandbox   //自定义Html模块

&powertoy=tweakomatic //Msn界面设定

注意，每次添加一项，把他们加在地址栏上显示的地址后回车。

2、到自定义选项下的模块里选择该模块（一般在最下面）添加后，保存。

3、对该新添加的模块进行参数设置。（详解如下） －－[进入详细教程](http://spaces.msn.com/members/wanganqi/Blog/cns!1pJ1K_K4vT2HZW460c2pa0ag!1195.entry)－－

新的自定义Html模块实现置顶日志

这就不用说了， 加上自己的代码就好了，可以先用微软的编辑器编辑下：

http://spaces.msn.com/RteCode.aspx?mkt=zh-hk －－[进入详细教程](http://spaces.msn.com/members/wanganqi/Blog/cns!1pJ1K_K4vT2HZW460c2pa0ag!1194.entry)－－

MSN 界面设定

要是要熟悉颜色代码，这里有提供详细的颜色代码如下：（或一个有趣的颜色代码页）

http://spaces.msn.com/members/wanganqi/Blog/cns!1pJ1K_K4vT2HZW460c2pa0ag!155.entry

－－[进入详细教程](http://spaces.msn.com/members/wanganqi/Blog/cns!1pJ1K_K4vT2HZW460c2pa0ag!1193.entry)－－