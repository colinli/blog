---
title: 喧嚣之隅的一片静谧－广仁寺
date: 2006-04-16T02:53:12+00:00
categories:
  - 生活
tags:
  - Jerry
  - 广仁寺 
---
![](/images/2006/广仁寺.jpg)

又是一个周末，正午的阳光在经历了沙尘和春雨后，显得有些疲惫。可爱的孩子，一定想和我一起下楼去玩耍，想要告诉我他从他喜欢的《小车迷》上又认识了几种汽车的标志。

 的确，孩子的世界是单纯而有快乐的，他兴奋的告诉我哪个车是宝马，哪个车是桑塔纳，看来他是认定了哪个ＶＷ就是桑塔纳，虽然我确信那辆一定是帕萨特。遗憾的是，他一直想要给我找一辆”福特“🚗，可竟然没有看到一辆。我想他一定向我一样遗憾，虽然我知道，他一定是认识的。
就这样，我们走到了城墙边，进了玉祥门，看到一条青石道，人和车都很少，于是就选择了安静。
![](http://tk.files.storage.msn.com/x1p4JHjVbcjTC-PNeqfvsNgk1EenpttwBehSdR7CKpExImQtwxy0BU8tJHuq47qMbYOk351AW-7-mS03oMCsSfOaAIhV4KoV0iZ4DEI4y6rdWz4GW12cvSLGl_TzGvqe4OISRLstp6y8HQqaXSXsVHUaA)

到城墙的西北隅，看到一个古色的院落，这里便是“广仁寺”了。
![](http://tk.files.storage.msn.com/x1p4JHjVbcjTC-PNeqfvsNgk1EenpttwBehSdR7CKpExIm9oIxGbl_pNhmFfdVwzfpY3UroyjyMVaJM4gOP93CzuF7kF2VtMcikafyR_wCy_PRhHZuraLS1KgM3falOn_kraMwvOUgbjDzPi4q7YM5U3g)

从寺的特点上看，与一般的寺院似有不同，看寺志，原来广仁寺是一个藏传佛教密宗黄教的喇嘛寺院，建于清康熙４４年（公元１７０５年），是西藏，青海，蒙古等地活佛进京朝觐，途径西安的行宫。
![](http://tk.files.storage.msn.com/x1p4JHjVbcjTC-PNeqfvsNgk1EenpttwBehSdR7CKpExIkpgsPbCUliewngx1WnHe_sbNaIZ3Xz_rJ6YMsBj8YnA3VE-p4VpyigMoAA0IcTPh7PHOOqIMSr6x6_3HJwr1UY6ieMlVID_MO8poQ5Pdyzsw)

我对宗教没有研究，但是从寺内的一些供奉上还是多少能看出文化融合的痕迹，比如在西殿，供有关公的塑像，这我想和藏传佛教应该是没有太大关联的吧，在前殿的弥勒佛和后殿的宗喀巴无论是在服饰上还有风格上都显得有些迥异。

寺内非常的静谧，在城区这样的喧嚣之中，对比尤为鲜明，古朴的中国传统式建筑，庄重肃穆的佛教氛围，让人的心绪变的平和与安静。