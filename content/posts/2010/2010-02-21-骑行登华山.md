---
title: "骑行华阴登华山"
date: 2010-02-21T05:34:00+00:00
categories:
  - 户外
tags:
  - 华山
  - 爬山
  - 骑行
---
![][山顶]

## 计划

过年，在家宅了些日子，看着天气有些好起来，就想骑一趟华山。我的单车买了都一个多月了，只是用来上下班这一点距离，它着实也感觉有些委屈了吧。  

有这个想法有些时日了，之所以想去趟华山，有诸多原因，重要的一点我是希望能在路过渭南时能稍作停留，因为那十年前被封藏的记忆，十年，如果可以成行，那么这将是十年后的第一次重温，那段有些清苦却快乐甜蜜的记忆。

计划是2天完成行程的，计划第一天早7点出发，下午的4点多左右到达玉泉院，然后稍事休整后，8点左右开始上山于翌日凌晨1点左右到达东峰投宿。第二日6点从东峰用半小时完成最高峰南峰的冲顶，最晚7点开始从南峰下山，并在11点半左右到达山下，计划12时开始返程，并于第二日的晚上9点半左右完成全部的计划。整个路程设置三个Milestone，10:00 到达临潼，12:00到渭南，15:00到罗敷，基本上每个点之间的距离平均在40KM左右，以便于控制整个进度。

装备上，最初的想法是重装，虽说华山已经成为非常成熟的景点，但是我还是希望完全不依靠景区的服务完成，无奈货架还没装，帐篷也不住家，只得放弃，也算是一个小小遗憾。  

服装方面，穿了一套排汗内衣、一条速干裤和一件抓绒外套，另外带了一件冲锋衣和一套羽绒衣裤，以及头灯和灶具，带了点路上喝的水，整套下来在5KG多一点。因为骑行和登山都要兼顾，所以穿了一双透气性要好些但是防水稍差的相对轻便一点的登山鞋。功课做好看看表已经是凌晨一点多了。

## 出发

揉了揉眼睛看看表，已经是9点多了，:-( ，出发时比计划晚了近三个小时了。结果走的匆忙，连我写着纸上的路线计划都忘记带了。导致的结果就是有2次把方向弄错，在进入银桥大道前多绕了4km的冤枉路。由于出发晚，所以为了赶时间第一个阶段稍微有些用力，这有些忌讳，因为肌肉还没有完全的适应，所以11:30到达临潼后就感觉到腿部肌肉开始有些酸痛，这才是刚刚开始，我有些担心，后面的路程该怎么完成。安慰自己说实在不行就骑到渭南然后就回来算了。

12:45到达新丰，早饭没吃，加上过年一路的饭馆大都关门，好容易看到了一家，就赶紧钻了进去，这时腿疼得更厉害，我把双腿贴到了炉子上，感觉能好些。  

到达渭南，我在我们熟悉的地方伫立，记忆的浮尘掸去，我就又看到春日里，我们那些曾经影像，时而清晰时而模糊，在十年之中的时空里交错纵横，以至于我变得有些意识模糊，忘记了今夕何夕。

或许是已经活动开了，或许是因为注意力的转移，腿感觉不再那么疼，于是按照计划继续下一个目的地-罗敷。过了渭南，路就变得起伏多了些。最终到达玉泉院时已经是晚上7点多了，下了车，寒冷让腿疼变的更加剧烈，刚一下车就一屁股坐倒在路边的路基上，紧紧抱着双腿在胸前。看来今晚上山的计划需要放弃了。坚持和放弃是一种人生最终极的智慧吧，在该坚持的时候坚持，在该放弃的时候放弃，这说起来及其简单的道理，实践起来确实无比的困难，我想起来关于珠峰的那些文章里的描述，很多人在体能无法支撑的情况下坚持登顶，但是却没有了下撤的体力而永远长眠，我想起了中学的一片作文题目，一个剖面图，一个人挖了好几口井，有好几口已经离水很近了，但是放弃了。所以我的原则就是如果是意志的问题就不放弃，如果是身体的因素就坚决放弃，但是这又是何其难哉。不是原理的事情，而是完全的经验了，所以我坐了很久，在坚持和放弃之间抉择，在是意志还是体能之间抉择。可在我连迈开双腿都变得有些艰难的时候，这样的抉择也变得有些无意义了。

## 华山

睁开眼睛，8点多了，暖暖的被窝让我赖了很久，这时，一个声音说再休息一天吧，另一个声音说不要放弃，我活动了一下双腿，感觉好了很多，我认为这是意志力的问题，于是收拾了一下，开始了。

10:00到达山门，开始了正式的路程，路已经很熟悉了，景也很熟悉了，所以这一天基本上都是在走路，计划13:00到达北峰，15:00到达极顶南峰。前一段路人比较少，到达北峰时，因为索道的原因，人一下多了起来，熙熙攘攘，喧喧闹闹。  

实际到达南峰是在15:15左右，因为中间把水杯忘在了一个休息点，又回去找耗费了些时间。15:30开始正式下山。  

下山时为了赶点时间，另外也是想第一次走下那条智取华山道，结果由于冰雪道路关闭，只好原路返回。19:02按照计划准时到达山门。不过说实在的，这些台阶修得太好了，雪扫的太净了，相比我更喜欢走那些覆盖着雪的，长满了野草的荒蛮的小路，那种感觉是我喜欢的。 

![山门][山门]

## 返回

第三日，听着一路喜欢的音乐，向着家的方向，快乐的骑行，当一切变得逐渐的熟悉，当夕阳掩去最后一抹的红晕，当焰火在空中次第盛开，而我已完成了全部的行程。


## 路线信息

### 计划

#### D1

- 3H 7:00 - 10：00到达临潼 40KM  
- 2H 10:00-12:00 达到渭南市 34KM 30m午餐  
- 2.5H 12:30-15:00 到达罗敷 43KM  
- 1H 15:00-16:00 到达玉泉院 15KM  
- 17:00 上山  
- 23：00 宿中峰  

#### D2  

- 7:00 下山  
- 9:00 到北峰  
- 11：00 骑行返回 
- 20：00 到西安  

### 实际执行

#### D1

- 9:40 出发  
- 11:30 46.4KM 临潼  
- 14:30 76KM  渭南  
- 17:50 124KM 罗敷  
- 19:30 145KM 玉泉院  

#### D2

- 10:00 玉泉院  
- 13:00 北峰  
- 15:10 南峰  
- 16:00 北峰  
- 19:00 山门  

#### D3  

- 10:00 出发
- 20:00 到达

骑行里程共计288.3KM。 

[山门]: /images/2010/骑行华山-山门.jpg
[山顶]: /images/2010/骑行华山-山顶.jpg