---
title: 樱桃谷的樱桃和草甸的杜鹃
date: 2010-05-26T14:46:16+00:00
categories:
  - 生活
tags:
  - 樱桃沟
  - 骑行
  - 吐槽
---

周末，几个同事相约踩单车去狄寨樱桃园，因为钱几年去过几次，所以轻车熟路。到了之后，比我想象的还要混乱，窄小的道路上拥挤着各式各样的车，或是逆行，或是灵活的穿插，直到形成一个绝妙的状态，大家都无法挪动，然后就是悦耳的喇叭，可是这和我有什么关系，像一条游鱼，欢快的游移在车洋之中。

到了任家庄的广场，你根本无法想象这是远离市中心的远郊，恍惚中好像进入闹市，一片车的海洋，可是这和我有什么关系呢，像一阵风，沿着陡弯的坡路，飘像谷底。

没有去农家乐，因为农家已经不乐。

值得回味的是从西河到毛西的那段小路，那条15年前，那个少年曾踩着一辆二六的飞鸽自行车，在一个春日的周末，曾经愉悦的漂行，记忆里，没有这么多车，只有路两旁的杨树，静静的开花，或是远远眺望着，塬上那绯红一片的春花。

已经没有兴趣在去整理手机里那几张无心而拍的照片，好在朋友发了，也写了，粘上，或许，就会忘了。  
http://community.active.com/blogs/staff/2010/05/24/ironkids-fishing-tour-of-california  
哦，对了，不是说樱桃谷的樱桃么，怎么光车，没樱桃了，买的樱桃没有挑，拿回来一看，都是放了好几天的，或是烂了或是霉了，所以都扔了。

好久没有进山了，我自己都不知道，我可以这么久都不进山，因为什么？说出来自己都觉得矫情，但是，是这样的，说因为工作和家庭，也许吧，主要还是因为这个季节太好了，这个天气太好了，这个风景太好了，不是矫情有是什么？不是么，户外需要包容心，因为山是自然给所有人的恩赐，每个人都有权利享受这自然的恩赐，不是我不能包容，而是我无法包容自己，无法让自己看到那可爱的山，那可爱的水，在可爱的人们的脚下变得无助而凄然。

古城上一篇“高山草甸还美么？“的帖子，让我更加感觉到一种发自内心的恐惧，这中恐惧之深，之甚，连我自己都无法去理解。如果一身户外装备，就可以称为驴的话，那么还是不要做个叫驴了吧，那么就像所有用些小工具破破密码就可以把自己叫做hacker一样，听起来也是多么的可笑。在我的意识中，能称为驴者，必是那种对于自然心怀无限的敬畏和虔诚，懂得生命和尊重生命，坚韧不拔而又懂得何时去放弃，还有重要的，起码的一点，就是不要去破坏，不要去践踏。

高山杜鹃很美，美的是那样的摄人心魄，开放在那远山之上，即便是严冷的冬季，也不肯退去衣装，你如果是一位少女，我会是如何的为你而动情，你的风骨，你的刚柔，从第一次的邂逅，就成了心中永久的情节。草甸，杜鹃，现在却像一个被沦落的少女，在老鸨和爪牙的淫威下，接受着一轮轮的玷污和糟蹋，透过茫茫的夜，遥望杜鹃盛开的地方，似乎能看到浓重的阴云下，月光不曾撕透的暗夜中，那美丽可人的少女，眼中噙满了泪，用手轻轻拉起被撕破的绢纱，试图掩起那如玉肌肤，却不经意间露出那道道的伤痕。  
http://bbs.269.net/thread-1642032-1-1.html 感谢清雨的文字和图片，