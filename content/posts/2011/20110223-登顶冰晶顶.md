---
title: 登顶冰晶顶

date: 2011-02-22T21:25:27+00:00
categories:
  - 生活 
tags:
  - 秦岭
  - 冰晶顶
  - 徒步
---
![Title][雪松]

>冰晶顶在秦岭山脉中按照海拔算是第三高的山峰了，海报3015米。除了太白，鳌山上三千的可能也就有冰晶顶了，太白和鳌山都已经走过，所以也一直想走一趟冰晶顶。朋友X刚去过，无意中说到这个愿望，于是决定陪我重上冰晶顶。

冰晶顶位于西安市户县南部东涝河上游朱雀国家森林公园最高处，可以从朱雀森林公园进，但是要买门票。所以徒步多选择从涝峪口进去的营盘沟上。冰晶顶本地人也叫静峪脑，最高点海拔3015米，从冰晶顶上去可以从太平森林公园出去，这条穿越路线在西安户外也是一条经典的路线，但一般是2天线路，而一天的线路则多以登顶原路返回为主。

## 营盘沟 

早上7点半从西安出发，走西汉高速，从朱雀下，然后继续向朱雀森林公园方向行驶，到达营盘沟附近，8:30左右开始上山，海拔1396米。有林场路可以走，路况要好些，但是要多走1km左右，所以从小路上，沿着河床缓坡向上。随着海拔的升高，路边的树上已经可以看到雾凇还有树挂了，恰如千树梨花。

![山沟][山沟]

然后就是一直的拔高，到达2523米海拔时，就到达了山脊，路稍微缓一些，接着就是穿过一片茂密的箭竹林，接着下一个很陡的坡，眼前就看到了一个铁丝护栏，穿过去就到达景区，说是景区，其实也很荒蛮，除了隐约可见的一些铁丝拉起的边界，和一两处写着‘未开发区域‘的简陋的告示牌，很难想象这里是景区，不是么？我们的眼中，景区应该是什么？平整的阶梯，熙攘的人群，玲琅满目的小卖部。嗯，别开发了。这就挺好。

## 小木屋

在往上，是一片较平的坡地，只是因为有雾，远处什么都看不见，到达一个荒废的小木屋，停下来歇歇脚，12点多了，简单吃了点干粮，就继续向前，小木屋海拔2643，离顶峰还有不到500米的高程。

雾依然很大，路比山谷中的路要平坦一些，植物也都变成了相对低矮的针叶树木，接近峰顶，树木变得更低矮，由于四周毫无遮挡，风可以肆无忌惮的肆虐，雪在树上就这样被风任着性子的戏弄，呈现出了奇异的景象。

## 峰顶

到达峰顶是在2：20，历时约6个小时。风有些大，天空中似被吹开一个洞隙，阳光就无力的倾泻了片刻，洞隙就再次消失，远处的山像水中的蛟龙，苍茫的背脊在飘散游移的雾气中，飘忽不定。

坐在第四纪冰川遗留下来的石海上，喝着冰冻的啤酒，和朋友欢呼着。

3点时分开始下撤，由于积雪和冰，下山变得比上山更加的艰难，即便是带着冰爪。因为雪，冰和石头混在一起，以至于朋友的冰爪崩了两侧的尖齿，而我的冰爪也被石头顶歪了，有些路段干脆坐在地上直接滑了下来。

## 林场路

到达林场岔路，天色已昏暗起来，开了头灯，在夜色中穿越最后的密林，在7：30左右终于到达徒步终点。整个线路耗时约11小时。包含休息，平均速度在1.3km/h，这是投影距离，实际时速应该在2-3km/h以上。  

同行 xy,hz

## 经验

共享秦岭登山徒步中的一些经验，既然是经验就有主观性，  
仅供参考。实际中应根据线路和各人不同情况有所调整。  

1. 一小时起伏不大可以辨认的山路通常可以走3-4KM。  
2. 连续的上坡，通常每小时可以拔高300米，根据地形  
情况和各人情况实际应有所调整。  
3. 连续的下坡，一小时通常可以降低600米，根据地形情况和  
各人情况实际应有所调整。  
4. 地图上1公里，实际路程会有2公里到3公里。  
5. 越深的沟，沟口越宽。一个10km深的沟，沟口腰部的  
宽度不会只有100米，可能接近1000米。  
6. 沟尾和梁尾通常比较陡峭，沟头和梁头通常比较平缓。  
这意味着大多数登顶的线路都是先走沟再走梁，而大多数  
下山的线路都是先走梁再走沟。

## 参考

- http://bbs.269.net/viewthread.php?tid=1898519&highlight=%2B%BD%B4%C6%CC 💀
- http://bbs.269.net/misc.php?action=viewthreadmod&tid=1898519 💀

[雪松]: /images/2011/冰晶顶-雪松.jpg
[山沟]: /images/2011/冰晶顶-山沟.jpg