---
title: 用EXIF信息批量rename
date: 2012-12-11T13:36:39+00:00
categories:
  - 技术
tags:
  - EXIF

---
照片文件默认是IMGxxx，或DSC-xxx这样的格式。有时候想按照拍摄时间来浏览的话发现不是很方便。

想用EXIF信息作为文件名，这样在排序时候可以按照时间顺序，同时加上设备名，这样管理起来比较方便。

找了几个批量改名工具用着不是很方便，刚好最近在用php帮朋友做个小应用，趁着手写了个小工具做。

```php
#!/usr/bin/php<?php
 
$old=$argv[1];
$ext=pathinfo($old,PATHINFO_EXTENSION);
 
$exif_data=exif_read_data($argv[1]);
#print_r($exif_data[DateTimeOriginal]);
@$new=$exif_data[DateTimeOriginal]."_".$exif_data[Model];
$new=str_replace(":","-",$new); //EXIF format 2010:06:18 13:03:25
$new=str_replace(" ","_",$new);
$new=strtolower($new.".".$ext);
rename($old,$new);
print$old."-->".$new."\n";
?>
```

运行结果就这样的

```
$ for x in `ls` ;do ../exif.php $x;done 
DSC_9170.jpg-->2011-04-17_13-59-36_nikon_d40.jpg 
DSCN0054.jpg-->2010-06-18_13-03-25_coolpix_l2.jpg 
DSCN8492.jpg-->2011-03-12_14-27-07_coolpix_l2.jpg 
DSCN8770.jpg-->2011-04-03_13-32-45_coolpix_l2.jpg
```