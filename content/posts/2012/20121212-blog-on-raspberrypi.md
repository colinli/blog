---
title: 把blog跑到树莓派上 
author: kaolizi

date: 2012-12-12T14:18:38+00:00
categories:
  - 技术
tags:
  - RaspberryPI
  - WordPress
  - blog
---

![](/images/2012/blog-rasppi.jpg)

RaspPi是一个信用卡大小的仅售35美元的Linux计算机，它主要目的是为了给小孩子教编程。由英国的一个慈善基金会开发。

从2006年有这个想法，到2011年8月12日第一个Alpha板子成功运行，2012年的4月开始接受订单,这个玩意儿确实引起了不小的反响。在youtube上各路n人展示着自己的各种创意。

从pi的官网指定的销售商[Element14](http://www.ickey.cn/raspberrypi.php)订购了一块，298多人民币。最近没时间折腾，所以先刷了个官方的debian系统，然后就想着把我的blog从国外free虚拟主机迁回来。运行还算稳定，但是不久就被伟大的墙给认证了。

用PI来host我的blog好处在于数据安全，不用担心丢数据。第二就是省电，功耗只有3w左右，一个月也就2-3度电。

以前在blogspot还有live space上写的一些东西在微软宣布终止livespace的时候迁移到了自己的blog上，一直不太愿意迁到新浪，疼讯什么的是因为这些不靠谱，真的不靠谱。 

不想续费给我的域名kaolizi.com，所以想弄个ddns，没有用需要实名认证的花生壳，选了changeip.com的 25u.com ，一个原因就是无需找什么客户端，直接支持url提交。在首页注册一个帐号，选一个自己喜欢的二级域名就可以了。然后在pi里面放一个cront任务来运行脚本。脚本内容如下：

```bash
#!/bin/bash

if [ ! -f /tmp/old ];
then
    echo "noip" >/tmp/old
fi

s1=`cat /tmp/old`
#echo $s1
s2=`curl -s http://ip-addr.es`
if [ $s1 != $s2 ];
then
    echo $s2 >/tmp/old
curl -s 'https://www.changeip.com/dynamic/dns/update.asp?u=xxxxxxx&p=xxxxxxxx&cmd=update&set=1&offline=0' >/tmp/update.txt
    /usr/bin/sendemail -f xxxx@gmail.com -t xxx@gmail.com \
    -u "$s1 to $s2" -o message-file=/tmp/update.txt
fi
```

首先检查自己的ip地址和上次相比是否已经改变，如果改变就通过curl提交新的ip，由于使用了路由器进行拨号，pi得到的地址是一个私有地址，因此需要通过ip-addr.es这个链接来获取真实地址。