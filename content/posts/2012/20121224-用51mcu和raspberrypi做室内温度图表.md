---
title: 用51mcu连接RaspberryPI做室内温度图表
date: 2012-12-23T17:03:00+00:00
categories:
  - 技术
tags:
  - RaspberryPI
  - MCU
  - rrdtool
---

### 思路

天冷了，房间里的暖气感觉一直不是很暖和。最近在搞cacti，对时序数据库rrd做了一些了解。在cacti里面根据历史数据可以根据刻/时/日/周/月/年的粒度来绘制历史图表。由于将数据进行了压缩，可以用很小的文件存储数年的历史趋势数据。所以就想用rrdtool来给室内温度做个曲线，来看下这个室温的历史曲线。 

二关于数据采集方面，前一段买了一个51单片机，里面刚好带了一个温度传感器。这样就解决了数据采集和呈现的部分。另外51上带了led发光管，同时可以将温度显示出来。

二处理方面，因为电脑不可能一直开机，所以理所当然的用功耗更低的RaspPI了，至此概念上没什么问题，就差实现了。

### 获取温度传感器数据

了解了一下51mcu的硬件结构将传感器连接到51，将以下的c代码编译生成bin，下载到单片机上。

```c
#include <reg52.h>
#include <stdlib.h>
#define uchar unsigned char
#define uint unsigned int
unsigned char flag,a,i;

uchar code tablet[]="";

unsigned char code table[]={0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,
                        0x07,0x7f,0x6f,0x77,0x7c,0x39,0x5e,0x79,0x71};
unsigned char code table1[]={0xbf,0x86,0xdb,0xcf,0xe6,0xed,0xfd,
                        0x87,0xff,0xef};

sbit DS=P2^2;          //define interface
uint temp;             // variable of temperature
sbit dula=P2^6;
sbit wela=P2^7;
sbit beep=P2^3;

void init()
{
TMOD=0X20;
TH1=0xfd;
TL1=0xfd;
TR1=1;
REN=1;
SM0=;
SM1=1;
EA=1;
ES=1;
}

// Temprature functions
void delay(uint count)      //delay
{
  uint i;
  while(count)
  {
    i=200;
    while(i>)
    i--;
    count--;
  }
}
void dsreset(void)       //send reset and initialization command
{
  uint i;
  DS=;
  i=103;
  while(i>)i--;
  DS=1;
  i=4;
  while(i>)i--;
}

bit tmpreadbit(void)       //read a bit
{
   uint i;
   bit dat;
   DS=;i++;          //i++ for delay
   DS=1;i++;i++;
   dat=DS;
   i=8;while(i>)i--;
   return (dat);
}

uchar tmpread(void)   //read a byte date
{
  uchar i,j,dat;
  dat=;
  for(i=1;i<=8;i++)
  {
    j=tmpreadbit();
    dat=(j<<7)|(dat>>1);  //读出的数据最低位在最前面，这样刚好一个字节在DAT里
  }
  return(dat);
}

void tmpwritebyte(uchar dat)   //write a byte to ds18b20
{
  uint i;
  uchar j;
  bit testb;
  for(j=1;j<=8;j++)
  {
    testb=dat&0x01;
    dat=dat>>1;
    if(testb)     //write 1
    {
      DS=;
      i++;i++;
      DS=1;
      i=8;while(i>)i--;
    }
    else
    {
      DS=;       //write 0
      i=8;while(i>)i--;
      DS=1;
      i++;i++;
    }

  }
}

void tmpchange(void)  //DS18B20 begin change
{
  dsreset();
  delay(1);
  tmpwritebyte(0xcc);  // address all drivers on bus
  tmpwritebyte(0x44);  // initiates a single temperature conversion
}

uint tmp()               //get the temperature
{
      float tt;
      uchar a,b;
      dsreset();
      delay(1);
      tmpwritebyte(0xcc);
      tmpwritebyte(0xbe);
      a=tmpread();
      b=tmpread();
      temp=b;
      temp<<=8;             //two byte compose a int variable
      temp=temp|a;
      tt=temp*0.0625;
      temp=tt*10+0.5;
      return temp;
}

void display(uint temp)            //显示程序
{
   uchar A1,A2,A2t,A3;
   A1=temp/100;
   A2t=temp%100;
   A2=A2t/10;
   A3=A2t%10;
   dula=;
   P0=table[A1];        //显示百位
   dula=1;
   dula=;

   wela=;
   P0=0x7e;
   wela=1;
   wela=;
   delay(1);

   dula=;
   P0=table1[A2];        //显示十位
   dula=1;
   dula=;

   wela=;
   P0=0x7d;
   wela=1;
   wela=;
   delay(1);

   P0=table[A3];        //显示个位
   dula=1;
   dula=;

   P0=0x7b;
   wela=1;
   wela=;
   delay(1);
}
// end temp functions

void main()
{
    init();
    while(1)
    {
        tmpchange();
        for(a=10;a>;a--)
        {
            display(tmp());
        }

         if(flag==1)
         {
            ES=;
 // SBUF=hex2dec(tmp());
                SBUF=tmp();
                while(!TI);
                    TI=;
                    SBUF=a;
                    while(!TI);
                    TI=;
                    ES=1;
                    flag=;
         } 

    }

}

void ser() interrupt 4
{
RI=;
a=SBUF;
flag=1;
}
```


### 在RaspPI上用python读取温度

由于c51上输出的温度是不带小数点的十六进制整型量，所以需要进行一个数值转换。 注意的是由于默认的PI上的usb转串口芯片自动识别后会被设置root和dailout组，并且是750的权限，因此需要修改

```
/lib/udev/rules.d/91-permissions.rules
#SUBSYSTEM==”tty”, GROUP=”dialout”
SUBSYSTEM==”tty”, GROUP=”sudo”
 
crw-rw—T 1 root sudo 188, 0 Dec 23 11:44 /dev/ttyUSB0
```

这样就可以被sudo组的用户使用了。
PI可以直接识别无需驱动。

```
[ 239.349670] usb 1-1.2.2: new full-speed USB device number 9 using dwc_otg
[ 239.471543] usb 1-1.2.2: New USB device found, idVendor=1a86, idProduct=7523
[ 239.471574] usb 1-1.2.2: New USB device strings: Mfr=0, Product=2, SerialNumber=0
[ 239.471592] usb 1-1.2.2: Product: USB2.0-Serial
[ 239.475632] ch341 1-1.2.2:1.0: ch341-uart converter detected
[ 239.479287] usb 1-1.2.2: ch341-uart converter now attached to ttyUSB0
```

gettmp.py

```python
#!/usr/bin/python
import serial 
import os 
if os.path.exists('/dev/ttyUSB0'): 
  port='/dev/ttyUSB0'
elif os.path.exists('/dev/ttyUSB1'): 
  port='/dev/ttyUSB1'
else:
  print"No comm port found" 
  os._exit(1) 
  ser=serial.Serial(port,9600,timeout=2) 
  ser.write('0') 
  x=ser.readline()
  #return hex format 
  with integerprintfloat(ord(x[]))/10 
  ser.close()
```

### 用rddtool绘图

rrdtool非常方便，直接用shell就可以处理数据的收集和绘图。

```shell
#!/bin/sh

rrdtool=/usr/bin
gettmp=/home/cli/lab/gettmp.py

RRD_DB_DIR=/home/cli/lab/
RRD_DB_FILE=/home/cli/lab/temp.rrd
RRD_DB_PIC=/home/cli/lab/
NOW=`date +%s`
MONTH_IN_SEC=2629743
MONTH_AGO=$(($NOW-$MONTH_IN_SEC))

ONE_QUARTER_AGO=$(($NOW-900))
ONE_HOUR_AGO=$(($NOW-3600))
ONE_DAY_AGO=$(($NOW-86400))
ONE_WEEK_AGO=$(($NOW-604800))
ONE_MONTH_AGO=$(($NOW-2629743))
ONE_YEAR_AGO=$(($NOW-31556926))

do_rrdgraph(){

if [ $2 = "quarter" ]
    then
    echo quarter
    flag=MINUTE:1:MINUTE:3:MINUTE:3::"%H:%M"
elif [ $2 = "hour" ]
    then
    echo hour
    flag=MINUTE:3:MINUTE:6:MINUTE:6::"%H:%M"
elif [ $2 = "day" ]
    then
    echo day
    flag=MINUTE:30:HOUR:1:HOUR:2::"%H:%M"
elif [ $2 = "week" ]
    then
    echo week
    flag=DAY:1:DAY:2:DAY:1::"%m/%d"
elif [ $2 = "month" ]
    then
    echo month
    flag=DAY:1:DAY:3:DAY:3::"%m/%d"
elif [ $2 = "year" ]
    then
    echo year
    flag=MONTH:1:MONTH:2:MONTH:2::"%Y/%m"
fi

if [ !$3 = "" ]
then
    echo "resolution=$3"
    resolution="-S $3"
else
    echo "resolution=300"
    resolution="-S 300"
fi

$rrdtool/rrdtool graph "${RRD_DB_PIC}temp_$2.png" -s $1 -h 120 -w 500 -a PNG ${resolution} -t "Temperature C (last $2)" \
-v bytes \
-R light \
-G mono \
-u 40 \
-l  \
--color BACK#000000 \
--color FRAME#00FF00 \
--color CANVAS#000000 \
--color GRID#000000 \
--color MGRID#003300 \
--color FONT#00FF00 \
--lower-limit= \
--units-exponent  \
--rigid \
--x-grid ${flag} \
DEF:a="$RRD_DB_FILE":temp:AVERAGE \
DEF:b="$RRD_DB_FILE":temp:MAX \
DEF:c="$RRD_DB_FILE":temp:MIN \
AREA:a#00EE00:"Temp. \: " \
GPRINT:a:AVERAGE:"%sAvg %.2lf" \
GPRINT:b:MAX:"%sMax %.2lf" \
GPRINT:c:MIN:"%sMin %.2lf"

}

if [ -e "$RRD_DB_FILE" ]
then
    echo "file exits: $RRD_DB_FILE"
    VALUE=$($gettmp)
echo $VALUE
    $rrdtool/rrdtool updatev $RRD_DB_FILE N:${VALUE}

    do_rrdgraph $ONE_QUARTER_AGO quarter
    do_rrdgraph $ONE_HOUR_AGO hour
    do_rrdgraph $ONE_DAY_AGO day 600
    do_rrdgraph $ONE_WEEK_AGO week 600
    do_rrdgraph $ONE_MONTH_AGO month 600
    do_rrdgraph $ONE_YEAR_AGO year 1800

else

$rrdtool/rrdtool create $RRD_DB_FILE --step 300 \
DS:temp:GAUGE:600::40 \
RRA:AVERAGE:.5:1:1200 \
RRA:MIN:.5:12:2400 \
RRA:MAX:.5:12:2400 \
RRA:AVERAGE:.5:12:2400
echo "file created:$RRD_DB_FILE"

fi
```
### 最终这样的

* 硬件是这样的

![](/images/2012/室温图-硬件.jpg)

* 跑了一段时间后从浏览器里打开可以看到这个图。从里面可以看到供暖后温度上升的趋势。
![](/images/2012/室温图-chart.png)
