---
title: 入了个Lenovo E135

date: 2013-03-19T13:54:59+00:00
categories:
  - 技术 
tags:
  - 笔记本
  - 数码
  - ArchLinux
---

![](/images/2013/e135.jpg)

amazon上买了个Thinkpad e135，配置不高，就是便携性比较好，比较轻。后来有有点后悔了，应该搞个大一点的，轻是轻了，可是我并不是会经常带着它出去，哎。验货发现有一个亮点，退又退不成，就这样算了。自从thinkpad弄到lenoveo手里，这个ibm苦心经营的牌子算是彻底给毁了。

自带的系统是ubuntu的，在这样配置的机器上跑是有点臃肿了。于是换了个Arch Linux，没有Gentoo那么2,但是比debian还有ubuntu好玩的多一些。
折腾折腾，基本上是该弄的都弄好了，用Drivel写了这篇blog。
用到的软件大致有：

```
dm: slim
de: openbox
pannel: lxpanel
term:guake
Pic view:gpicview
quick launch: Gnome-Do
```
