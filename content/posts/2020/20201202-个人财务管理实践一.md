---
title: 个人财务管理实践之一 概念
date: 2020-12-02
categories:
  - 生活
tags: 
  - 个人管理
---

## 序 

年轻时候对钱没什么概念，自己物欲也低也没刻意储蓄和理财的习惯，属于随性加佛系的金钱观。随着生活经历增加，开始意识到个人财务管理的重要性，也刻意的接触和了解了一些相关的知识。如时间管理概念类似，当我们需要合理的利用时间一样，首先需要对自己金钱的使用的情况做一个统计，然后进行分析。所以记账是在各类关于个人理财与财务规划的书籍和文章中最为基本的一个阶段。

从最早用excel记录自己的支出，到后来出现的app也用过一些，比如随手记，挖财这样的单式记账软件到现在使用基于复式记账的开源软件GnuCash，这中间前后也有快十年了，逐渐形成了一套适合自己的账务管理方式，经过改进觉得比较适应自己的各类需求，临近千禧后的第一个十年末，刚好整理和总结一下。

## 为什么要记账

个人理财的目标是实现个人财务自由，做自己想做的事情，而关于财务自由一个最简单的模型就是当被动收入大于自己的固定支出的时候，也就不用工作就可以维持正常的生活。这是基本的模型，考虑到其他的因素这个模型真正实现起来还是有不少额外因素需要考虑。

所以这里面有就有两个核心的数据，第一是自己的固定支出，也就是不管怎么样也无法避免的支出，像吃穿住行。而第二部分就是了解自己的收入，作为打工人，工资收入是主要来源，工资收入减去自己的固定支出，产生储蓄。然后利用储蓄的资金进行投资获取被动收入，然后让这个被动收入逐渐增加到能够平衡固定支出，从而实现所谓的财务自由，不再因为自己不再出卖劳动就可以获得维持生活的资金。 

因此第一部分就需要了解自己的固定支出是多少，这种情况下一个单式记账的app就比较适合，像挖财，随手记这些默认会建立好支出大类，然后发生支出时候将支出计入对应的类别就可以在一定的期间了解自己的支出情况。此类文章也很多，无须赘述。

## 为什么选择复式记账

什么是复式记账这个会计中最基础的概念，网上也是一大堆了，就说类似挖财这种的痛点吧。

1. 忘记记录，导致支出信息不准确。关键是自己并不知道忘记了。
2. 太琐碎，有时候几块几十块的支出懒得记，但是聚沙成塔，这种高频低金额的交易的处理也是很头疼，记了烦不计也烦。不胜其烦。

复式记账其实并不能解决第二个问题，我是用另外的方式来解决的，后表。而对于第一个问题，由于复式记账本身带有校验的这样的功能，所以即便是漏记也很容易发现。

## 除了记账还有什么

除了记账，个人账务系统的更重要的一个功能还在于帮助自己进行资产配置。人在不同的时期对财务的支配和需求是不同的，而金钱的时间价值和流动性也是相关的，时间越久汇报率也高，类似定期存款5年和1年的利率差异。因此根据自己的对钱的规划，可以将不同用途的钱放在不同的流动性产品中，可以在不影响生活的情况下更高的追求回报。 

所以在账户的设置上，更注重资产账户的设置。我是按照如下的科目来设置的。

```
流动资产
    现金
    活期
        微信零钱
        招行xxxx
        中行xxxx
        股票现金账户
    货币基金
投资型货币资产
    定期
    银行理财
    场外基金
        华泰-xxxx
        支付宝-xxxx
    股票与场内基金
        股票a
        股票b
    投资型保险
    社保账户
自用资产
```

这样的好处在于通过GnuCash的资产负债表可以清楚的看到自己资产流动性和比例，比如如果流动性高的活期比例高，说明收益会低，而如果投资型资产中比例过高可能短期内应对不确定性支出的能力会降低，每个月去根据自己的生活目标和计划来调整资产的配置做到不至于过于被动。这些功能挖财也有，但是由于这些app会把信息上传到云端，从隐私和安全性之前也没有在这类app上记录资产信息。毕竟现在什么都实名感觉不太放心。而GnuCash作为一个电脑端软件，所有信息都存放在本地，没这个担忧。

## 除了Gnucash还有什么软件

[GnuCash](https://www.gnucash.org) 是一个GUI的软件，跨平台，也可以在Linux下运行，也是开源里面很成熟的产品。除了个人财务也胜任一些小企业的记账需求包括员工管理和账龄发票等，不过毕竟是国外的开发者，都是基于国外的财务规范。 未必适应国内的情况。

另外还有一个比较Geek的[BeanCount](https://github.com/beancount/beancount) 只可惜知道的有点完，已经懒得再去折腾，不然很可能开始就用这个完全基于纯文本和命令行的基于Python的工具了。

## 财务恒等式

按照会计学的原理，一共有五个基本的账户目型和不严格的定义。

1. 资产: 你拥有的
2. 负债： 你欠别人的
3. 所有者权益: 可理解为资产的净值
4. 收入: 资产增加
5. 支出: 资产减少

而这五个账户类型直接遵循如下的恒等式。

`资产-负债=所有者权益+(收入-支出)`

![恒等式](https://www.gnucash.org/docs/v4/C/gnucash-guide/figures/basics_AccountRelationships.png)

这对没有会计基础的理解起来会有些晦涩，特别是关于所有者权益这块儿。而在复式几张中使用的借和贷的符号也会有些误解，不过这个在Gnucash中可以设置，具体为“编辑"-首选项"-"科目”-“标签”取消使用会计卷标勾选。这样在资产类科目中会使用收到和花费来取代借和贷方标记。

举例说明的话，就是2020年1月1日那天，自己有只有现金1000元，其中有100是借同事的。到2020年12月31日，年收入10,000元，并且全年各种花费5,000元,那么对应上述公式。

`6000-100=900+(10000-5000)`

可以理解为在年初的时候自己拥有的1000元现金由于100元的债务，所以真正属于自己的资产净值为900元。

到一个期间的末尾12月31日这天。10,000收入-5,000的支出结余5,000元，加上期初的1000元，一共有6,000元现金，这6000属于资产，但是由于有100元为借款属于负债，6000-100=5900属于净资产，它刚好和收支结余的5000元加上年初的所有者权益也就是净资产刚好是相等的。

而到2021年1月1日，作为一个新的会计期间的开始，可以将收支差额结转到所有者权益中，即从2021年1月1日开始收入和支出科目归零，这结余的5000元计入所有者权益，意味着在新的一个期间，我的净资产从900增加到了5900）。这个过程也就是常说的结账。

有了上述这些基本的概念，就可以着手来建立一个个人的账务系统了。